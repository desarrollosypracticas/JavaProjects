import java.awt.*;

public class SemaforoGUI extends CVentanas {
	
	private CIniciar Semaforo1,Semaforo2, Semaforo3, Semaforo4; 
	private Image[] estadoSemf=new Image[4];
	private Image carro1, carro2,carro3,carro4;	
	private Image cruce, Imgsemaforo1, Imgsemaforo2, Imgsemaforo3, Imgsemaforo4;
	private int  X2, Y2, X1, Y1,X3, Y3, X4, Y4;	
	private Image casas[][] = new Image[4][4];
	private Image arboles[] = new Image[6];
	private String casitas[] ={"casa","casa","casa","casa"};	
	private String Ncasitas[] ={"1","","5","4"};
	private String arbolitos[] = {"arbol","arbol2","arbol","arbol2","arbol","arbol2"};
	private String semaforo[] = {"verde1.gif","amarillo1.gif","rojo1.gif","apagado.gif"};
	private CSonido sonido = new CSonido();
	private Graphics G;
	private Image I;
	private Dimension D;	
	private MediaTracker Mt;
	private int pista;
	
	public SemaforoGUI()
	{	
		super("semaforo", 0, 0, 0, 0, Color.gray, true);
		Mt = new MediaTracker(this);
		
		cargarsemaf();
		cargarImagenes();
		CargarCasitas();
		CargarArboles();		
	
		X2 = 40;	Y2 = 350;
		X1 = 480;	Y1 = 50;		
		X3 = 520;	Y3 = 735;	
		X4 = 820;	Y4 = 304;
		
		Iniciar();	
		
		this.setResizable(false);
		this.setVisible(true);		
	}
	private void cargarImagenes() {		
			
		cruce = Toolkit.getDefaultToolkit().getImage("edificios\\cruce.png");
		
		carro1 = Toolkit.getDefaultToolkit().getImage("carros\\arrabj.gif");
		carro2 = Toolkit.getDefaultToolkit().getImage("carros\\abjarr2.gif");		
		carro4 = Toolkit.getDefaultToolkit().getImage("carros\\derizq4.gif");
		carro3 = Toolkit.getDefaultToolkit().getImage("carros\\izqder.gif");		

		Imgsemaforo1 = estadoSemf[3];
		Imgsemaforo2 = estadoSemf[3];
		Imgsemaforo3 = estadoSemf[3];
		Imgsemaforo4 = estadoSemf[3];

	}
	public void cargarsemaf() {
		for (int i = 0; i < semaforo.length; i++) 
		{
			estadoSemf[i] = Toolkit.getDefaultToolkit().getImage("edificios\\"+
					semaforo[i]);
			
			Mt.addImage(estadoSemf[i],i);
		}				
	try{Mt.waitForAll();}
	catch (InterruptedException e){};
	}	
	public void CargarCasitas()
	{		
		for (int i = 0; i < casitas.length; i++)		
			for (int j = 0; j < Ncasitas.length; j++) 
			{
				casas[i][j] = Toolkit.getDefaultToolkit().getImage("edificios\\"+
						casitas[(int)(Math.random()*casitas.length)]+Ncasitas[(int)(Math.random()*Ncasitas.length)]+".gif");
				Mt.addImage(casas[i][j],i);
			}				
		try{Mt.waitForAll();}
		catch (InterruptedException e){};		
	}
	public void CargarArboles()
	{		
		for (int j = 0; j < arboles.length; j++) 
			{
				arboles[j] = Toolkit.getDefaultToolkit().getImage("edificios\\"+
						arbolitos[(int)(Math.random()*arbolitos.length)]+".gif");
				Mt.addImage(arboles[j],j);
			}				
		try{Mt.waitForAll();}
		catch (InterruptedException e){};		
	}
	public void Iniciar()
	{	
		Semaforo1 = new CIniciar("AV. Martires de cananea    ",6000,2000,1000);
		Semaforo2 = new CIniciar("AV. Universidad            ",4000,2000,1000);
		Semaforo3 = new CIniciar("AV. Martires de rio Blanco ",1000,2000,1000);		
		Semaforo4 = new CIniciar("Calle del Tecnologico      ",6000,2000,1000);
		
		Semaforo1.start();		
		Semaforo2.start();
		Semaforo3.start();
		Semaforo4.start();		
	}
	
	public void paint(Graphics g){update(g);}
	public void update(Graphics g)
	{	
		Dimension d = getSize();

	    if(G == null || d.width != D.width || d.height != D.height)
	    {	D = d;
	        I = createImage(D.width, D.height);
	        G = I.getGraphics();
	    }
		Graphics2D g2 = (Graphics2D) G;
		sonido.PlayAudio(pista);
		    //de arriva a abajo
			g2.drawImage(carro1,X1, Y1+CIniciar.Y2, this);
			//de izquierda a derecha
			g2.drawImage(carro3,X2+CIniciar.X, Y2, this);			
			//de abajo a arriva
		    g2.drawImage(carro2,X3, Y3-CIniciar.Y, this);		    
			//de derecha a izquierda		    
			g2.drawImage(carro4,X4-CIniciar.X2, Y4, this);			
			
			g.drawImage(I, 0, 0, this);			
			g2.drawImage(cruce,0,0,this.getWidth(),this.getHeight(),this);
			
			for (int i = 0; i < casitas.length; i++)
				for (int j = 0; j < Ncasitas.length; j++) {
					g2.drawImage(casas[i][j],75+60*j,20+40*i,50,80,this);
				}
			for (int i = 0; i < casitas.length; i++)
				for (int j = 0; j < Ncasitas.length; j++) {
					g2.drawImage(casas[i][j],735+60*j,20+40*i,50,80,this);
				}
			for (int i = 0; i < casitas.length; i++)
				for (int j = 0; j < Ncasitas.length; j++) {
					g2.drawImage(casas[i][j],75+60*j,550+40*i,50,80,this);
				}
			for (int i = 0; i < casitas.length; i++)
				for (int j = 0; j < Ncasitas.length; j++) {
					g2.drawImage(casas[i][j],735+60*j,550+40*i,50,80,this);
				}
			for (int i = 0; i < arbolitos.length; i++)
				g2.drawImage(arboles[i],30+50*i,250+0*i,50,50,this);
			for (int i = 0; i < arbolitos.length; i++)
				g2.drawImage(arboles[i],30+50*i,480+0*i,50,50,this);
			for (int i = 0; i < arbolitos.length; i++)
				g2.drawImage(arboles[i],710+50*i,250+0*i,50,50,this);
			for (int i = 0; i < arbolitos.length; i++)
				g2.drawImage(arboles[i],710+50*i,480+0*i,50,50,this);
			
		
		g2.drawImage(Imgsemaforo1,650, 216,this);		
		g2.drawImage(Imgsemaforo2,650, 510,this);		
		g2.drawImage(Imgsemaforo3,368, 510,30,60,this);		
		g2.drawImage(Imgsemaforo4,368, 216,this);
		EstadosSemaforo();
		repaint();
		
		try{Thread.sleep(15);}
    	catch(InterruptedException ie){}
	}
	public void EstadosSemaforo(){
		if(Semaforo1.isAlive())
		{pista = 1;
			if(CIniciar.getestado() == 0)
				Imgsemaforo1=estadoSemf[0];
			else if(CIniciar.getestado() == 1)
			{
				if(Imgsemaforo1 == estadoSemf[0])
					Imgsemaforo1 = estadoSemf[3];
				else
					Imgsemaforo1 = estadoSemf[0];
				
				repaint();				
			}
			else if(CIniciar.getestado() == 2)
				Imgsemaforo1=estadoSemf[1];
			//intercasemaforos();
			
			CIniciar.AvanzaY();			
		}		
		if(!Semaforo1.isAlive())
		{Imgsemaforo1=estadoSemf[2];		
		}
		
		if(Semaforo2.isAlive()&&!Semaforo1.isAlive())		
			{
				if(CIniciar.getestado() == 0)
					Imgsemaforo2=estadoSemf[0];
				else if(CIniciar.getestado() == 1)
				{
					if(Imgsemaforo2 == estadoSemf[0])
						Imgsemaforo2 = estadoSemf[3];
					else
						Imgsemaforo2 = estadoSemf[0];
					
					//repaint();					
				}
				else if(CIniciar.getestado() == 2)
					Imgsemaforo2=estadoSemf[1];			
				CIniciar.AvanzaX();			
			}	
		if(!Semaforo2.isAlive())
		{	Imgsemaforo2=estadoSemf[2];
		}	
		
		if(Semaforo3.isAlive()&& !Semaforo2.isAlive())
		{
			if(CIniciar.getestado() == 0)
				Imgsemaforo3=estadoSemf[0];
			else if(CIniciar.getestado() == 1)
			{
				if(Imgsemaforo3 == estadoSemf[0])
					Imgsemaforo3 = estadoSemf[3];
				else
					Imgsemaforo3 = estadoSemf[0];
				
				repaint();
				
			}
			else if(CIniciar.getestado() == 2)
				Imgsemaforo3=estadoSemf[1];
			CIniciar.AvanzaY2();
			
		}
		if(!Semaforo3.isAlive())
			Imgsemaforo3=estadoSemf[2];
				
		if(Semaforo4.isAlive() && !Semaforo3.isAlive())
		{
			if(CIniciar.getestado() == 0)
				Imgsemaforo4=estadoSemf[0];
			else if(CIniciar.getestado() == 1)
			{
				if(Imgsemaforo4 == estadoSemf[0])
					Imgsemaforo4 = estadoSemf[3];
				else
					Imgsemaforo4 = estadoSemf[0];
			
				repaint();
				
			}
			else if(CIniciar.getestado() == 2)
				Imgsemaforo4=estadoSemf[1];
			CIniciar.AvanzaX2();			
			
		}		
		if(!Semaforo4.isAlive())
		{Imgsemaforo4=estadoSemf[2];			
			Iniciar();
		}		
	}
	public void retardo(int pTiempo)
    {   try { Thread.sleep(pTiempo); } catch (Exception e) {}
    } 
	public static void main(String[] args){new SemaforoGUI();
	}
}