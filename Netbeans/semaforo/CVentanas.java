import java.awt.*;
import javax.swing.*;

public class CVentanas extends JFrame 
{
	Dimension pantalla=Toolkit.getDefaultToolkit().getScreenSize();	
	public CVentanas()
	{
		this(null,0,0,0,0,Color.gray,false);
	}
	public CVentanas (String pTitulo)
	{
		this(pTitulo,0,0,200,100,Color.gray,false);
	}
	public CVentanas(String pTitulo, int pX, int pY)
	{
		this(pTitulo,pX,pY,200,100,Color.gray,false);
	}
	
	public CVentanas(String pTitulo, int pX, int pY, int pAncho, int pLargo)
	{
		this(pTitulo,pX,pY,pAncho,pLargo,Color.gray,false);
	}

	public CVentanas(String pTitulo, int pX, int pY, int pAncho, int pAlto,Color pcolor, boolean pPrincipal) 
	{
		
		this.setTitle(pTitulo);			
		this.setSize(getMaximumSize().width, getMaximumSize().height);
		this.setBackground(pcolor);
		this.setLocation(pX,pY);		
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		if (pPrincipal)
			this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		else
			this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		
	}
	
	/**public class CCerrarPrincipal extends WindowAdapter
	{
		public void windowClosing(WindowEvent pEvento)
		{
			System.exit(0);
		}
		public class CCerrarSEcundaria extends WindowAdapter
		{
			public void windowClosing(WindowEvent pEvento)
			{
				dispose();
			}
		}
	}*/
	
	
}
