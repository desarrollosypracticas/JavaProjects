public class CIniciar extends Thread
{   
	public static int Y,Y2;
	public static int X,X2;	
	private String[] Nombre={"VERDE","VERDE","AMARILLO","ROJO"};	
	private int verde, amarillo, parpaverde;
	private boolean activo;
	private static int estado=-1;
	private static int aTiempo[];
	
	
	public CIniciar(String pNombre,int pVerde,int pParpaverde,int pAmarillo)
    {	
		super(pNombre);
	    verde = pVerde;
	    parpaverde = pParpaverde;
		amarillo = pAmarillo;		
		aTiempo = new int[] { verde, parpaverde, amarillo, 0 };
		activo = true;    	
    }
    public void run()
    {	
    	estado();    		  		   
    }
    public void estado()
    {
    	synchronized(System.out)
	    {
	    	if(activo)
	    	{	
	    		estado = ++estado % 4; 	    		
	    		activo = (estado != 3);
				System.out.print("\n Carril "+getName()+"el semaforo esta en: "+Nombre[estado]+" "+aTiempo[estado]);				
				retardo(aTiempo[estado]); 	
				estado();
	    	}
	    	
	    }    	
    }
    public static int getestado()
    {
    	return estado;
    }
   
    public static void AvanzaX()
    {
    	if(X+30>1000) X=0; else X +=26;  
    }
    public static void AvanzaY()
    {	
    	if(Y+30>795)Y=0; else Y+=10;    
    }
    public static void AvanzaX2()
    {    	
    	if(X2+30>1000) X2=0; else X2 +=15;    	 
    }
    public static void AvanzaY2()
    {	
    	if(Y2+30>795)Y2=0; else Y2+=10;    
    }    
    
    public void retardo(int pTiempo)
    {   try { Thread.sleep(pTiempo); } catch (Exception e) {}
    }    
   
     
}