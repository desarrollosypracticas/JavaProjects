/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejm.sockettcpserver;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Jose Luis
 */
public class EjmSocketTCPServer {

    
    public static void main(String[] args) {
        try {
            
            //Creamos el socket
            ServerSocket ss=new ServerSocket(80);
            System.out.println("Socket TCP iniciado...");
            //Crear un ciclo para aceptar las peticiones que envie el cliente
            while(true){
                Socket socket=ss.accept();
                //Obtenerlos datos de entrada
                //DataInputStream in=new DataInputStream(socket.getInputStream());
                BufferedReader in=new BufferedReader(new InputStreamReader(socket.getInputStream()));
                PrintWriter out=new PrintWriter(socket.getOutputStream());
                //Con esto obtenemos los datos de entrada
                System.out.println("Ip: "+socket.getInetAddress());
                System.out.println("Puerto: "+socket.getPort());
                System.out.println("Mensaje: "+in.readLine());
                
                //headers
                out.println("HTTP/ 1.0 200 OK");
                out.println("Content-Type: text/html; charset-utf-8");
                out.println("Server: Mini server Programa tu mismo.");
                out.println("");
                //Código html
                out.println("Hola mundo, mensaje desdeprograma tu mismo");
                out.close();
            }//End while
        
        } catch (IOException ex) {
            Logger.getLogger(EjmSocketTCPServer.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
}
