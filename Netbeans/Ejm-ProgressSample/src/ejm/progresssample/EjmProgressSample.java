/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejm.progresssample;

import java.awt.BorderLayout;
import java.awt.Container;
import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JProgressBar;
import javax.swing.border.TitledBorder;

/**
 *
 * @author Jose Luis
 */
public class EjmProgressSample {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        JFrame f=new JFrame("JProgressBar Sample.");
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        Container content=f.getContentPane();
        JProgressBar progressBar=new JProgressBar();
        progressBar.setValue(70);
        progressBar.setStringPainted(true);
        TitledBorder border=BorderFactory.createTitledBorder("Reading...");
        progressBar.setBorder(border);
        content.add(progressBar,BorderLayout.NORTH);
        f.setSize(300,100);
        f.setVisible(true);
    }
    
}
