/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package creararboles;

/**
 *
 * @author Jose Luis
 */
public class Carbol {
    String nombre,zona;
    int altmedia;

    public int getAltmedia() {
        return altmedia;
    }

    public void setAltmedia(int altmedia) {
        this.altmedia = altmedia;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getZona() {
        return zona;
    }

    public void setZona(String zona) {
        this.zona = zona;
    }

    public Carbol() {
    }

    public Carbol(String nombre, String zona, int altmedia) {
        this.nombre = nombre;
        this.zona = zona;
        this.altmedia = altmedia;
    }

    @Override
    public String toString() {
        return "nombre= " + nombre + "altmedia= " + altmedia;
    }
    
    
}
