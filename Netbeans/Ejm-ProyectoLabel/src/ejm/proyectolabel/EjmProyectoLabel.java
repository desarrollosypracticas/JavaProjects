/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejm.proyectolabel;

import java.awt.FlowLayout;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

/**
 *
 * @author Jose Luis Elizalde P
 */
public class EjmProyectoLabel extends JFrame{

    private JLabel label1;
    private JLabel label2;
    private JLabel label3;
    
    public EjmProyectoLabel(){
        super("Prueba Label.");
        setLayout(new FlowLayout());
        
        label1=new JLabel("Label 1");
        label1.setToolTipText("Esta es label1");
        add(label1);
        
        Icon bug=new ImageIcon(getClass().getResource("icon.png"));
        label2=new JLabel("Label con texto e imagen.");
        label2.setIcon(bug);
        label2.setToolTipText("Esta es label2");
        add(label2);
        
        label3=new JLabel();
        label3.setText("Label con texto e imagen abajo");
        label3.setIcon(bug);
        label3.setHorizontalTextPosition(SwingConstants.CENTER);
        label3.setVerticalTextPosition(SwingConstants.BOTTOM);
        label3.setToolTipText("Esta es label3");
        add(label3);
    }//End of constructor
    public static void main(String[] args) {
        EjmProyectoLabel ob=new EjmProyectoLabel();
        ob.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        ob.setSize(550,750);
        ob.setVisible(true);
    }
    
}
