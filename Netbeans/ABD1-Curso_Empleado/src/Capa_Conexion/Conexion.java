/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Capa_Conexion;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author Jose Luis
 */
public class Conexion {
    
    
    //Creamoslavariable connetion
    static Connection cn=null;
    //Creamos la clase conexion
    
    public static Connection Enlace(Connection cn) throws SQLException{
    //Ruta de la base de datos la cual crearemos
    String ruta="E:\\Archivos Compartidos\\Desarrollos\\NetBeansProjects\\ABD1-Curso_Empleado\\src\\Databases\\backup.db";
    
        try {
            Class.forName("org.sqlite.JDBC");
            cn=DriverManager.getConnection("jdbc:sqlite:"+ruta);
            
        } catch (ClassNotFoundException ex) {
            JOptionPane.showMessageDialog(null,"Ocurrio exception: "+ex);
            Logger.getLogger(Conexion.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return cn;
    
    
    }
    
    
}//End of class
