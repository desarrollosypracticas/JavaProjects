/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Capa_Operaciones;




//Importamos el paquete de conexion
import Capa_Conexion.Conexion;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class Operaciones {
    
    //Creamos las variables para la conexion
    static Connection cn;
    static Statement s;
    static ResultSet rs;
    DefaultTableModel modelo=new DefaultTableModel();
    //Creamos la operación para mostrar datos en una jtable en el jform
    
    
    public DefaultTableModel lista(){
    
        try {
            cn=Conexion.Enlace(cn);
            Statement s=cn.createStatement();
            //Consulta a mostrar
            String query="SELECT * FROM empleado;";
            rs=s.executeQuery(query);
            ResultSetMetaData rsmd=rs.getMetaData();
            //Obtenemos número de columnas
            int CanColumns=rsmd.getColumnCount();
            //Comprobamos
            for (int i = 1; i < CanColumns+1; i++) {
                //Cargamos columnas en modelo
                modelo.addColumn(rsmd.getColumnLabel(i));
            }//End for
            
            while(rs.next()){
               //Creando array
               Object[] fila=new Object[CanColumns];
               //Cargando datos a modelo
                for (int i = 0; i < CanColumns; i++) {
                    fila[i]=rs.getObject(i+1);
                }//End for
                
                modelo.addRow(fila);
                }//End while
        } catch (SQLException ex) {
            Logger.getLogger(Operaciones.class.getName()).log(Level.SEVERE, null, ex);
        }
    //Retornando modelo para el JTable
   
    
    return modelo;
     }//End of method lista
    


    public DefaultTableModel lista2(){
    
        try {
            cn=Conexion.Enlace(cn);
            Statement s=cn.createStatement();
            //Consulta a mostrar
            String query="SELECT * FROM cursando;";
            rs=s.executeQuery(query);
            ResultSetMetaData rsmd=rs.getMetaData();
            //Obtenemos número de columnas
            int CanColumns=rsmd.getColumnCount();
            //Comprobamos
            for (int i = 1; i < CanColumns+1; i++) {
                //Cargamos columnas en modelo
                modelo.addColumn(rsmd.getColumnLabel(i));
            }//End for
            
            while(rs.next()){
               //Creando array
               Object[] fila=new Object[CanColumns];
               //Cargando datos a modelo
                for (int i = 0; i < CanColumns; i++) {
                    fila[i]=rs.getObject(i+1);
                }//End for
                
                modelo.addRow(fila);
                }//End while
        } catch (SQLException ex) {
            Logger.getLogger(Operaciones.class.getName()).log(Level.SEVERE, null, ex);
        }
    //Retornando modelo para el JTable
   
    
    return modelo;
     }//End of method lista
        
    //Creando método para insertar datos
    public void AgregarConsulta(String id,String nombre,String apellido,String direccion,String telefono,String nif,String nac,String nacionalidad,String sexo,String firma,int salario){
        //Previniendo errores
        try {
            
            Statement s=cn.createStatement();
            String query="INSERT INTO empleado     VALUES ('"+id+"','"+nombre+"','"+apellido+"','"+direccion+"','"+telefono+"','"+nif+"','"+nac+"','"+nacionalidad+"','"+sexo+"','"+firma+"',"+salario+");";
            s.executeUpdate(query);
            s.close();
            cn.close();
            JOptionPane.showMessageDialog(null,"Agregado");
            
        } catch (SQLException ex) {
            Logger.getLogger(Operaciones.class.getName()).log(Level.SEVERE, null, ex);
        }
        
   
    }//End method AgregarConsulta
    
    
    public void EliminarConsulta(String id){
        
        try {
            Statement s=cn.createStatement();
            String query="DELETE FROM empleado WHERE codigo_empleado='"+id+"'";
            s.executeUpdate(query);
            s.close();
            cn.close();
            JOptionPane.showMessageDialog(null,"ELIMINADO");
        } catch (SQLException ex) {
            Logger.getLogger(Operaciones.class.getName()).log(Level.SEVERE, null, ex);
        }




}//End method RenombrarColumna
    
public void RenombrarColumna(String nombre,String nuevo){
        
        try {
            Statement s=cn.createStatement();
            String query="ALTER TABLE empleado RENAME COLUMN "+nombre+" to "+nuevo+";";
            s.executeUpdate(query);
            s.close();
            cn.close();
            JOptionPane.showMessageDialog(null,"ELIMINADO");
        } catch (SQLException ex) {
            Logger.getLogger(Operaciones.class.getName()).log(Level.SEVERE, null, ex);
        }




}//End method EliminarConsulta    


    //Creamos método para modificar datos
    public void ModificarConsulta(String nombre,String apellido,String id,String direccion,String telefono,String nif,String nac,String nacionalidad,String sexo,String firma,int salario){
        
        try {
            Statement s=cn.createStatement();
            String query="UPDATE empleado SET nombre='"+nombre+"',apellido='"+apellido+"',direccion='"+direccion+"',telefono='"+telefono+"',nif='"+nif+"',fecha_nac='"+nac+"',nacionalidad='"+nacionalidad+"',sexo='"+sexo+"',firma='"+firma+"',salario="+salario+" WHERE codigo_empleado="+id+"";
            s.executeUpdate(query);
            s.close();
            cn.close();
            JOptionPane.showMessageDialog(null,"Modificado");
        } catch (SQLException ex) {
            Logger.getLogger(Operaciones.class.getName()).log(Level.SEVERE, null, ex);
        }
    
    
    }//End method ModificarConsulta
    
    public void triggerrollback(){
        
        
        
        //Inicia trigger
        try{
            Statement tr=cn.createStatement();
            String query=""
                    + "CREATE TRIGGER roll UPDATE OF cursando ON backup \n" +
                    "  BEGIN\n" +
                    "  UPDATE orders SET address = new.address WHERE customer_name = old.name;\n" +
                    "  END;"
                    ;
        
        }catch(SQLException ex){
            Logger.getLogger(Operaciones.class.getName()).log(Level.SEVERE,null,ex);
        }
        
    
    }//End Triggerrollback
    
}   //End

