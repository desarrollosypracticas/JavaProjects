/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;



/**
 *
 * @author Jose Luis
 */
public class EjmSQLiteConexion {


    
    public static void main(String[] args) {
        //Se crea la instancia a objeto y se conecta a SQLite
        SQLite_conexion fbc=new SQLite_conexion();
        //Se insertan algunos datos
        fbc.insert("persona","nombre,apellido","'Charly','Manson'");
        fbc.insert("persona","nombre,apellido","'Marilyn','Garcia'");
        fbc.insert("persona","nombre,apellido","'Marcelo','Chamboneti'");
        //Se imprimen los datos de la tabla
        System.out.println(fbc.select());
        fbc.desconectar();
    }
    
}
