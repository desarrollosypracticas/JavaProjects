/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;


class SQLite_conexion {
    
        private Connection connection=null;
    private ResultSet resultSet=null;
    private Statement statement=null;
    private String db="E:\\Archivos Compartidos\\Desarrollos\\NetBeansProjects\\Ejm-SQLite-Conexion\\src\\Database\\dbTest.sqlite";
    
    //Constructor de clase que se conecta a la base de datos SQLite
    
    public SQLite_conexion(){
        
        try {
            Class.forName("org.sqlite.JDBC");
            connection=DriverManager.getConnection("jdbc:sqlite:"+this.db);
            System.out.println("Conectado a la base de datos SQLite ["+this.db+"");
            
            
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(EjmSQLiteConexion.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println(ex);
        } catch (SQLException ex) {
            Logger.getLogger(EjmSQLiteConexion.class.getName()).log(Level.SEVERE, null, ex);
        }
    
    
    }//Constructor
    
    
    /*
    Metodo para insertar un registro en la base de datos
    INPUT: 
    table=nombre de la tabla
    fields=String con los nombres de los campos donde insertar Ej.: campo1,campo2,campo_n
    values=String con los datos de los campos a insertar Ej.: valor1,valor2,valor_n
    OUTPUT:
    Boolean
    */
    
    public boolean insert(String table,String fields,String values){
        boolean res=false;
        //Se llama la consulta
        String query="INSERT INTO "+table+"("+fields+") VALUES ("+values+")";
        //Se ejecuta la consulta
        try{
        PreparedStatement pstm=connection.prepareStatement(query);
        pstm.execute();
        pstm.close();
        res=true;
        }catch(Exception e){
                System.out.println(e);
                
                }//End try-catch
    
    return res;
    }//End of method insert
    
    /*
    Médoto para realizar una consulta a la base de datos
    INPUT:
    OUTPUT: String con los datos concatenados
    */
    public String select(){
        
            String res=" ID | Nombre | Apellido \n";
          
             
        try {
            statement = (Statement) connection.createStatement();
            resultSet = statement.executeQuery("SELECT * FROM persona ; ");
            while(resultSet.next()){
                res+=resultSet.getString("id")+" | "+resultSet.getString("nombre")+" | "+resultSet.getString("apellido")+"\n";
                
            
            
            }//End while
        } catch (SQLException ex) {
            Logger.getLogger(EjmSQLiteConexion.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println(ex);
        }
        
            
         
    return res;
    } //End method select
    
    //Método para desconectar
    public void desconectar(){
        
        try {
            resultSet.close();
            statement.close();
            connection.close();
            System.out.println("Desconectado de la base de datos ["+this.db+"]");
            
        } catch (SQLException ex) {
            Logger.getLogger(EjmSQLiteConexion.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println(ex);
        }
    
    }//End method desconectar
    
    
}
