package poo_ejercicio1c;

import javax.swing.JOptionPane;

public class Organizador {
    Productos[] obj=new Productos[100];
    Productos cosa=new Productos();    
    int pos=0;
    void Menu(){        
        cosa.caja[0]=10000;
        cosa.caja[1]=0;
        cosa.caja[2]=0;
        boolean con=true;
        cosa.creador(obj);
        do{
            int op=Integer.parseInt(JOptionPane.showInputDialog(null,"1.Agregar Producto\n2.Consultar Productos\n3.Comprar Producto\n4.Vender Producto\n5.Ver inventario\n6.Ver Caja\n7.Salir","Menu",JOptionPane.QUESTION_MESSAGE));
            switch(op){
                case 1:
                    obj[pos].nombre=JOptionPane.showInputDialog(null,"Ingresa nombre del producto");
                    pos++;
                    obj[pos].compra=Integer.parseInt(JOptionPane.showInputDialog(null,"Ingresa costo del producto"));
                    pos++;
                    obj[pos].venta=Integer.parseInt(JOptionPane.showInputDialog(null,"Ingresa venta del producto"));
                    pos++;                   
                    obj[pos].cantidad=Integer.parseInt(JOptionPane.showInputDialog(null,"Ingresa cantidad del producto"));
                    pos++;
                    break;
                case 2:
                    cosa.Ver(obj);
                    break;
                case 3:
                    cosa.Comprar(obj);
                    break;
                case 4:
                    cosa.Vender(obj);
                    break;
                case 5:   
                    cosa.Ver_Prod(obj);
                    break;
                case 6:
                    cosa.Ver_Inv();
                    break;
                case 7:
                    con=false;
                    JOptionPane.showMessageDialog(null, "Gracias por su visita","Vuelva pronto",JOptionPane.INFORMATION_MESSAGE);
                    break;
                default:
                    JOptionPane.showMessageDialog(null, "Opcion no valida","Error",JOptionPane.ERROR_MESSAGE);
                    break;
            }
        }while(con);
    }
}
