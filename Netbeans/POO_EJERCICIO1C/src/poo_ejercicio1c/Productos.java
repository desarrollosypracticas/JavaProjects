package poo_ejercicio1c;

import javax.swing.JOptionPane;

public class Productos {
    String nombre;
    int venta,compra,cantidad;
    int caja[]=new int[3];
    void Ver(Productos x[]){           
        boolean con=true;
        String cad="Nombre P.compra P.Venta Cant.\n",aux2; 
        String aux=JOptionPane.showInputDialog(null,"Introduce el nombre");
        for (int i = 0; i <x.length && con==true; i+=4) {   
            
                if(aux.equals(x[i].nombre)){
                    cad=cad+x[i].nombre+"             ";
                    i++;
                    cad=cad+x[i].compra+"             ";
                    i++;
                    cad=cad+x[i].venta+"          ";
                    i++;
                    cad=cad+x[i].cantidad+"  ";
                    i++;
                    con=false;
                }
            }
           
        if(con){
            JOptionPane.showMessageDialog(null,"Lo siento producto no encontrado");
        }
        else{
            cad+="\n";            
        JOptionPane.showMessageDialog(null,cad);
        }
      
        
     
    }
    void Ver_Inv(){
        String cad=" ";
        for (int i = 0; i <caja.length; i++) {
            cad+=caja[i]+" ";
        }
     JOptionPane.showMessageDialog(null,"Caja Utilidades Perdidas\n"+cad);
        
    }
    void Comprar(Productos x[]){
        boolean b =false,p=false,s=false;
        String prod=" ";
        int cant=0,y=0;
        for (int i = 0; i < x.length; i+=4) {
            if (x[i]!=null) {
                b=true;
            }   
        }  
        if(b){
            int costo,a,z;
            prod=JOptionPane.showInputDialog(null,"Ingrese el nombre del producto","Registro",JOptionPane.QUESTION_MESSAGE);        
            while(y<x.length && s==false){  
                if(prod.equals(x[y].nombre)){
                    p=true;
                    s=true;
                    a=y+1;
                    z=y+3;
                    cant=Integer.parseInt(JOptionPane.showInputDialog(null,"Ingresa la cantidad de productos a comprar","Registro",JOptionPane.QUESTION_MESSAGE));                    
                    if(cant<=x[z].cantidad){
                        costo=cant*x[a].compra;
                        if(costo<=caja[0]){
                            caja[0]=caja[0]-costo;
                            caja[2]+=costo;
                            x[z].cantidad+=cant;
                            JOptionPane.showMessageDialog(null,"Producto comprado exitosamente");
                        }
                        else{
                            JOptionPane.showMessageDialog(null, "No hay dinero en caja","Registro",JOptionPane.WARNING_MESSAGE);                        
                        }                
                    }                
                }
                y+=4;
            }
            if(!p){
                JOptionPane.showMessageDialog(null, "No existen productos","Error",JOptionPane.ERROR_MESSAGE);
            }
        }
        else {
            JOptionPane.showMessageDialog(null, "No existen registros","Error",JOptionPane.ERROR_MESSAGE);
        }
        System.out.println(caja[0]);        
    }
    void Vender(Productos x[]){
        System.out.println(caja[0]);
        boolean b =false,p=false,s=false;
        String prod=" ";
        int cant=0,y=0;
        for (int i = 0; i < x.length; i+=4) {
            if (x[i]!=null) {
                b=true;
            }   
        }  
        if(b){
            int utilidad,aux,aux2,aux3=0,aux4=0,a,c;
            prod=JOptionPane.showInputDialog(null,"Ingrese el nombre del producto","Registro",JOptionPane.QUESTION_MESSAGE);        
            while(y<x.length && s==false){                
                if(prod.equals(x[y].nombre)){
                    p=true;
                    s=true;
                    a=y+1;
                    c=a+1;
                    int z=y+3;
                    cant=Integer.parseInt(JOptionPane.showInputDialog(null,"Ingresa la cantidad de productos a vender","Registro",JOptionPane.QUESTION_MESSAGE));
                    if(cant<=x[z].cantidad){
                        aux=x[a].compra;
                        aux2=x[c].venta;
                        utilidad=aux2-aux;  
                        aux3=utilidad*cant;                    
                        if(utilidad<0){
                            aux3=utilidad*cant*-1;
                            if(caja[0]<aux3){                                                                                    
                                JOptionPane.showMessageDialog(null, "Perdidas","Error",JOptionPane.ERROR_MESSAGE);
                            }
                            else{                            
                                caja[0]+=(aux3*-1);
                                caja[2]+=aux3; 
                                x[z].cantidad-=cant;
                            }                        
                        }
                        else{                       
                            caja[0]+=aux3;
                            caja[1]+=aux3;  
                            x[z].cantidad-=cant;
                        }                                                                                                                         
                    }
                }
                y+=4;
            }
            if(!p){
                JOptionPane.showMessageDialog(null, "No existen productos","Error",JOptionPane.ERROR_MESSAGE);
            }
        }
        else {
            JOptionPane.showMessageDialog(null, "No existen registros","Error",JOptionPane.ERROR_MESSAGE);
        }
        System.out.println(caja[0]);
        JOptionPane.showMessageDialog(null,"Producto vendido exitosamente");
    }    
    void Buscar(String x[][]){
       String resp=JOptionPane.showInputDialog(null,"Ingresa el nombre del producto"); 
       String cad="";
       boolean l=false;
        for (int i = 0; i < x.length; i++) {
            if(resp.equals(x[i][0])){
                l=true;
               for (int j = 0; j <x[i].length; j++) {  
                  cad+=x[i][j]+"  ";  
                }   
            }   
        }
        if (l==false) {
            JOptionPane.showMessageDialog(null,"Productos no existente","Error",JOptionPane.ERROR_MESSAGE);
        }
        JOptionPane.showMessageDialog(null,cad);
        
    }
    Productos [] creador(Productos x[]){
        int y=0;
        while(y<x.length){
            x[y]=new Productos();
            y++;
        }
        return x;
    }
    void Ver_Prod(Productos x[]){
        String cad="Producto Cantidad\n";
        int c=3;
        for (int i = 0; i <x.length; i+=4,c+=4) {
            if(x[i].nombre!=null){
             cad+=x[i].nombre+"             "+x[c].cantidad+"\n";   
            }
            
        }
           
           
       JOptionPane.showMessageDialog(null,cad);
}
    
}