package figuras;
import com.sun.xml.internal.ws.api.config.management.policy.ManagedServiceAssertion;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.KeyEvent;

public class Interfaz extends javax.swing.JFrame {

    public Interfaz() {
        initComponents();
        this.setLocationRelativeTo(null);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        triangulo = new javax.swing.JButton();
        circulo = new javax.swing.JButton();
        cuadrado = new javax.swing.JButton();
        Salir = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Dibujar Rosendo Efren Chavez Gami;o");
        addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                formKeyPressed(evt);
            }
        });

        triangulo.setFont(new java.awt.Font("Tahoma", 0, 3)); // NOI18N
        triangulo.setIcon(new javax.swing.ImageIcon("C:\\Users\\Ross\\Documents\\NetBeansProjects\\Figuras\\Figuras\\triangulo.jpg")); // NOI18N
        triangulo.setToolTipText("");
        triangulo.setMaximumSize(new java.awt.Dimension(100, 100));
        triangulo.setMinimumSize(new java.awt.Dimension(50, 50));
        triangulo.setPreferredSize(new java.awt.Dimension(50, 50));
        triangulo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                trianguloActionPerformed(evt);
            }
        });

        circulo.setIcon(new javax.swing.ImageIcon("C:\\Users\\Ross\\Documents\\NetBeansProjects\\Figuras\\Figuras\\circulo.jpg")); // NOI18N
        circulo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                circuloActionPerformed(evt);
            }
        });

        cuadrado.setIcon(new javax.swing.ImageIcon("C:\\Users\\Ross\\Documents\\NetBeansProjects\\Figuras\\Figuras\\cuadrado.jpg")); // NOI18N
        cuadrado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cuadradoActionPerformed(evt);
            }
        });

        Salir.setText("Salir");
        Salir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SalirActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 313, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(Salir))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addGap(98, 98, 98)
                        .addComponent(triangulo, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(88, 88, 88)
                        .addComponent(circulo)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 88, Short.MAX_VALUE)
                        .addComponent(cuadrado)
                        .addGap(59, 59, 59)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(33, 33, 33)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cuadrado)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(triangulo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(circulo)))
                .addGap(18, 18, 18)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(Salir)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void formKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_formKeyPressed
    if(evt.getKeyCode()==KeyEvent.VK_ENTER){
        System.exit(0); 
     }
    }//GEN-LAST:event_formKeyPressed

    private void SalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SalirActionPerformed
    System.exit(0); 
    }//GEN-LAST:event_SalirActionPerformed

    private void trianguloActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_trianguloActionPerformed
    Graphics k = jPanel1.getGraphics();
    Dibujo t = new Triangulo();
    t.dibujar(k);  
    }//GEN-LAST:event_trianguloActionPerformed

    private void circuloActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_circuloActionPerformed
   Circulo c = new Circulo();
   c.dibujar(jPanel1.getGraphics());
    }//GEN-LAST:event_circuloActionPerformed

    private void cuadradoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cuadradoActionPerformed
        Cuadrado cu= new Cuadrado();
        cu.dibujar(jPanel1.getGraphics());
    }//GEN-LAST:event_cuadradoActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Salir;
    private javax.swing.JButton circulo;
    private javax.swing.JButton cuadrado;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JButton triangulo;
    // End of variables declaration//GEN-END:variables
public interface Dibujo {
    void dibujar (Graphics g);
    
}
public class Cuadrado implements Dibujo{
 @Override
        public void dibujar(Graphics g) {
           g.setColor(Color.blue);
           g.fillRect(410, 30,120, 120);
        }
        }
public class Circulo implements Dibujo{
 @Override
        public void dibujar(Graphics g) {
            g.setColor(Color.red);
            g.fillOval(270, 30, 120, 120);
        }
        }
public class Triangulo implements Dibujo{
@Override
        public void dibujar(Graphics g) {
           g.setColor(Color.yellow);
           int x[]={50,150,250};
           int y[]={150,20,150};
           g.fillPolygon(x,y,3);
        }
        }
}
