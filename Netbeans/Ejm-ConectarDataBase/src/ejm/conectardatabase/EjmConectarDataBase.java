/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ejm.conectardatabase;

import com.sun.rowset.CachedRowSetImpl;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import javax.sql.rowset.CachedRowSet;
import jdk.nashorn.internal.ir.Statement;


public class EjmConectarDataBase {

    
    public CachedRowSet Function(String sql){
        try{
            Class.forName("Driver");
            
            String url="jdbc:motor:servidor:puerto/basedatos";
            Connection con=DriverManager.getConnection(url,"usuario","contraseña");
            Statement s=(Statement) con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
            
            ResultSet rs;
            rs = s.executeQuery(sql);
            
            CachedRowSet crs=new CachedRowSetImpl();
            crs.populate(rs);
            
            rs.close();
            s.close();
            con.close();
            
            return crs;
        }catch(Exception e){
            System.out.println(e.getMessage());
        }//End try...catch
        return null;
    }//End method
    
    public void StoreProcedure(String sql){
        try{
            Class.forName("Driver");
            
            String url="jdbc:motor:servidor:puerto/basededatos";
            Connection con=DriverManager.getConnection(url,"usuario","contraseña");
            Statement s=con.createStatement();
            
            s.execute(sql);
            
            s.close();
            con.close();
        }catch(Exception e){
            System.out.println(e.getMessage());
        }//End try...catch
    }//End StoreProcedure
    
    
    public static void main(String[] args) {
        
    }
    
}
