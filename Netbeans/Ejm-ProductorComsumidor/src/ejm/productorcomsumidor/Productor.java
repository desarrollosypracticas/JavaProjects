/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejm.productorcomsumidor;

import java.util.Random;

/**
 *
 * @author Jose Luis
 */
class Productor implements Runnable {

    private final Random aleatorio;
    private final Contenedor contenedor;
    private final int idproductor;
    private final int TIEMPOESPERA=1500;
    
    /*cONSTRUCTOR DE LA CLASE*/
    //@PARAM CONTENEDOR cONTENEDOR COMÚN A LOS CONSUMIDORES Y EL PRODUCTOR
    //@param odproductor Identificador del productor
    
    public Productor(Contenedor contenedor, int idproductor) {
        this.contenedor=contenedor;
        this.idproductor=idproductor;
        aleatorio=new Random();
        
        
    }//End constructor
    
    /*Implementación de la hebra.*/
    
    public void run(){
        while(Boolean.TRUE){
        int poner=aleatorio.nextInt(300);
        contenedor.put(poner);
        System.err.println("El productor "+idproductor+" pone:"+poner);
        try{
            Thread.sleep(TIEMPOESPERA);
            
        }catch(InterruptedException e){
            System.err.println("Productor "+idproductor+": Error en rum--->");
        }//End thy...catch
    }//End while
    }//End method run
    
}
