/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejm.productorcomsumidor;

/**
 *
 * @author Jose Luis
 */
class Contenedor {
    
    private int contenido;
    private boolean contenedorlleno=Boolean.FALSE;
    
    /*Obtiene de forma concurrente o síncroniza el elemento que hay en el contenedor
    */
    
    public synchronized int get(){
        while(!contenedorlleno){
            try{
                wait();
                
            }catch(InterruptedException e){
                System.err.println("Contenedor: Error en get--->"+e.getMessage());
            }//End try...catch
        }//End while
        
        contenedorlleno=Boolean.FALSE;
        notify();
        return contenido;
    }//End method get
    
    /*Introduce de forme concurrente o síncronizada un elemento en el contenedor */
    
    public synchronized void put(int value){
        while(contenedorlleno){
            try{
                wait();
                
            
            }catch(InterruptedException e){
                System.err.println();
            }//End try...catch
        }//End while
        contenido=value;contenedorlleno=Boolean.TRUE;
        notify();
        
    }//End method put
    
}
