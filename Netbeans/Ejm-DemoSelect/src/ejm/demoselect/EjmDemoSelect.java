/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ejm.demoselect;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 *
 * @author Jose Luis
 */
public class EjmDemoSelect {

    
    public static void main(String[] args) {
        //Parametros de la conexión.
        String usr="root";
        String pwd="";
        String driver="com.mysql.jdbc.Driver";
        String url="jdbc:mysql://localhost/prueba";
        
        Connection con=null;
        PreparedStatement pstm=null;
        ResultSet rs=null;
        
        try{
            /*
            Parte 1
            */
            
            //levanto el driver
            Class.forName(driver);
            
            //Establezco la conexión
            con=DriverManager.getConnection(url,usr,pwd);
            
            /*
             Parte 2
            */
            
            //Defino un query
            String sql="SELECT empno,ename,hiredate,deptno FROM emp";
            
            //Preparo la sentencia que voy a ejecutar.
            pstm=con.prepareStatement(sql);
            
            //Ejecuto la sentencia y obtengo los resultados en rs
            rs=pstm.executeQuery();
            
            //Itero los resultados (registros)
            while(rs.next()){
                //Muestro los campos del registro actual
                System.out.println(rs.getInt("empno")+", ");
                System.out.println(rs.getString("ename")+", ");
                System.out.println(rs.getDate("hiredate")+", ");
                System.err.print(rs.getInt("deptno"));
            }//End while
        }catch(Exception ex){
            ex.printStackTrace();
            throw new RuntimeException(ex);
        }//End try...catch
        finally{
            try{
                /*
                    Parte 3
                */
                
                //Cierro todos los recursos en orden inverso al que fueron adquiridos
                if (rs!=null) rs.close();
                if(pstm!=null) pstm.close();
                if(con!=null) con.close();
                    
                
            }catch(Exception ex){
                ex.printStackTrace();
                throw new RuntimeException(ex);
            }//End try...catch
        }//End finally
    }
    
}
