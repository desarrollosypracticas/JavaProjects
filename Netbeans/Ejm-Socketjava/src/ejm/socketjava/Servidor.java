/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejm.socketjava;

import java.awt.event.ActionListener;
import java.io.DataInputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextArea;

/**
 *
 * @author Jose Luis
 */
public class Servidor extends JFrame implements Runnable{
    
    JTextArea txtmensajes;

    
    /*Hacer un hilo, dalo que los hilos corren independientemente
    al programa y no afectan a su funcionamiento. Implementamos
    de Runnable*/
    
    public Servidor(){
        txtmensajes=new JTextArea();
        txtmensajes.setBounds(10,10,400,400);
        add(txtmensajes);
        
        setLayout(null);
    
        setSize(500,500);
        setVisible(true);
        
        
        
        
        
        //Llamar al run creando un hilo
        Thread hilo=new Thread(this);
        //El método start manda llamar al run
        hilo.start();
        
        
    }//End constructor
     
    public static void main(String args[]){
    
        new Servidor();
        
        
    
    }//End main

    
    
    //Método especial de Runnable
        //Se ejecuta en segundo plano
    @Override
    public void run() {
        try {
            //Abrir el puerto una sola ves  y siempre acepta llamadas
            //Abrimos servidor 9090, del cliente
            ServerSocket servidor=new ServerSocket(9090);
            //Creamos socket
            Socket cli;
            
            //Lo ponemos en un ciclo infinoto
            //para monitorizar siempre las llamadas entrantes
            while(true){
                
            //se detiene aquí hasta que llega una llamada    
            cli=servidor.accept();//aceptamos a quien mande
            
            //Recibes sus datos , entran datos por parte del cliente
            DataInputStream flujo=new DataInputStream(cli.getInputStream());
            
            //En variable guardamos lo que se ha enviado
            String msg=flujo.readUTF();
            //lo mandamos al jtextarea con append
            //para saber quien lo esta mandando aplicamos: 
            txtmensajes.append("\n"+cli.getInetAddress()+": "+msg);
            //Cerramos el socket cliente
            cli.close();
            
                if (msg.equalsIgnoreCase("FIN")) {
                    servidor.close();//Cerramos servidor
                    break;//Para romper al while 
                }//End if
            }//End while
            
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        
       
    }
    
    
    
}
