/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejm.socketjava;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.DataOutputStream;
import java.net.Socket;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextField;

/**
 *
 * @author Jose Luis
 */
public class Cliente extends JFrame implements ActionListener{
    
    JTextField txtmensaje;
    JButton btnenviar,btnsalir;

    
    public static void main(String[] args) {
        //Crear instancia para que se ejecute el constructor
        new Cliente();
        
        new Servidor();
    }
    
    
    //Constructor
    public Cliente(){
        //inicializar mensaje
        txtmensaje=new JTextField();
        //Ubicamos el texto(x,y,ancho,alto)
        txtmensaje.setBounds(10,10,200,20);
        //Agregar texto a formulario
        add(txtmensaje);
        
        //instanciar el boton
        btnenviar=new JButton();
        //enviar el texto
        btnenviar.setText("Enviar");
        //Ubicamos 
        btnenviar.setBounds(10,40,150,20);
        //Al boton de enviar le agregamos ActionListener
        btnenviar.addActionListener(this);
        //Agregamos al formulario
        add(btnenviar);
        
        
        //Creamos botón salir
        //instanciamos botón
        btnsalir=new JButton();
        //enviamos el texto
        btnsalir.setText("Salir");
        //Ubicamos
        btnsalir.setBounds(10,70,150,20);
        //Al botón de salir le agregamos ActionListener
        btnsalir.addActionListener(this);
        //Y agregamos al formulario
        add(btnsalir);
        
        //establecer propiedades del formulario
        setLayout(null);
        //
        setSize(400,400);
        setVisible(true);
    
    
    }//End constructor

    
    //Este método se va a ejecutar cada ves que se haga click
    //en un boton
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() ==btnenviar) {
            try{
                //Conectarme, creando socket(Address,port),//cualquier puerto libre
                //por ahora trabajar con mi propia maquina: 127.0.0.1
                //Si queremos enviar a otra pc ponemos nuestra IP
                /*Vamos al cmd y ponemos IPCONFIG*/
                Socket cli=new Socket("192.168.1.72",9090);
                //Salir datos de aquí,permite enviar datos, y enviamos la salida del cliente
                DataOutputStream flujo=new DataOutputStream(cli.getOutputStream());
                //Enviar datos
                flujo.writeUTF(txtmensaje.getText());
                //Cerramos el socket
                cli.close();
                
            
            }catch(Exception ex){
            System.out.println("Error en cliente: "+ex.getMessage());
            }//End try-catch
        }//End if
        
        if (e.getSource() ==btnsalir) {
            System.exit(0);
        }
    }
}
