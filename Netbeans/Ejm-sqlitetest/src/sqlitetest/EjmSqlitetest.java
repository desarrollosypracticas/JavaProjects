/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sqlitetest;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Jose Luis
 */
public class EjmSqlitetest {

    
    public static void main(String[] args) {
      
            try {
                Class.forName("org.sqlite.JDBC");
                
                String dburl="jdbc:sqlite:E:\\Archivos Compartidos\\Desarrollos\\NetBeansProjects\\Ejm-sqlitetest\\src\\DataBase\\Empleados.db";
                Connection conexion=DriverManager.getConnection(dburl);
                
                Statement Consulta=conexion.createStatement();
                ResultSet rs=Consulta.executeQuery("SELECT * FROM Empleado;");
                
                DefaultTableModel modelo=new DefaultTableModel();
                JTable tabla =new JTable(modelo);
                
                //Creo 3 columnas con sus etiquetas
                //estas son las columnas del JTable
                modelo.addColumn("Código");
                modelo.addColumn("Nombre");
                modelo.addColumn("Domicilio");
                
                while(rs.next()){
                    Object [] datos=new Object[4];//Crea un vector
                    //Para almacenar los valores del ResultSet
                    datos[0]=(rs.getInt(1));
                    datos[1]=(rs.getString(2));
                    datos[2]=(rs.getString(3));
                    System.out.println(rs.getString(2));
                    
                    //Añado el modelo a la tabla
                    modelo.addRow(datos);
                    //datos=null;//Limpia los datos del vector de la memoria
                    
                
                }//End while
                
                rs.close();//Cierra el ResultSet
                
                JFrame f=new JFrame();
            
                f.setBounds(10,10,300,500);
                f.getContentPane().add(new JScrollPane(tabla));
                
                f.setVisible(true);
                
                
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(EjmSqlitetest.class.getName()).log(Level.SEVERE, null, ex);
            } catch (SQLException ex) {
            Logger.getLogger(EjmSqlitetest.class.getName()).log(Level.SEVERE, null, ex);
        }
    
    
    
    

    }
    
    
    /*
     // DAR DE ALTA
	 try {
		 Class.forName("org.sqlite.JDBC");
 
		 String dburl = "jdbc:sqlite:/home/test/sqlitetest/Empleados.db";
		 Connection conexion = DriverManager.getConnection(dburl);
 
 
		 String id = idtxt.getText();
		 String nombre = nombretxt.getText();
		 String domicilio = domicilio.getText();
		
		 Statement Consulta = conexion.createStatement();
		 Consulta.executeUpdate("INSERT INTO Empleado " + "VALUES (11, txtid 'Mr.', 'Springfield', 2001)");
		
		 conexion.close();
    */
    
}
