
package ejm.funciones_trigonometrica;

import java.util.Scanner;

public class EjmFunciones_trigonometrica {

   
    
    
    public static void main(String[] args) {
        Scanner leer=new Scanner(System.in);
    
    final double PI=3.1416;
    int ang;
    double x;
    
    System.out.println("Ángulo (en grados): ");
    ang=leer.nextInt();
    x=ang*PI/180; //Convierte ang a radianes
    System.out.printf("sen (%d) = %.4f\n",ang,Math.sin(x));
    System.out.printf("cos (%d) = %.4f\n",ang,Math.cos(x));
    System.out.printf("tan (%d) = %.4f\n",ang,Math.tan(x));
    System.out.printf("cot (%d) = %.4f\n",ang,1/Math.tan(x));
    System.out.printf("sec (%d) = %.4f\n",ang,1/Math.cos(x));
    System.out.printf("csc (%d) = %.4f\n",ang,1/Math.sin(x));
    
    }//End method main
    
}
