/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto_tallerdb;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.Timer;

/**
 *
 * @author Jose Luis
 */
public class Login extends javax.swing.JFrame {
    Connection con=null;
    private boolean flag=false;
    private Timer tiempo;
    int cont;
    public final static int TWO_SECOND=5;
    String usr="",pass="",priv="",usrf="",privf="",nombre="";
    
    
    
    public Login() {
        initComponents();
        setLocationRelativeTo(null);
        //cargarusers();
        //privilegios();
        cargarusuariosbd();
        
       
       
    }
    
    public void cargarusuariosbd(){
        
        try {
            Class.forName("com.mysql.jdbc.Driver");
            con=DriverManager.getConnection("jdbc:mysql://localhost/tallerbd","root","");
        Statement st=con.createStatement();
        ResultSet rs=st.executeQuery("select Nickname,Privilegios from usuarios;");
        combousers.removeAllItems();
        while(rs.next()){
            combousers.addItem(rs.getString(1));
            
            
        }//End while
        //return rs.getString(1);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
        }
           
     
    }//End method cargarusuariostabla
    
    class TimerListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            cont++;
            barra.setValue(cont);
            
            //System.out.println(usr+", "+pass+","+priv);
            if(cont==100 /*&& priv.equalsIgnoreCase("Administrador")*/){
                    
                tiempo.stop();
                usr=combousers.getSelectedItem().toString();
                
                esconder();
                System.out.println("usuario:"+usr);
               // System.out.println(usr+","+pass+","+priv+": timer admin");
                administrador adm=new administrador();
                adm.admin(""+usr,""+priv,""+nombre);
                
                adm.setVisible(true);
                setVisible(false);
            //this.dispose();
            }
           /* if(cont==100 && priv.equalsIgnoreCase("Usuario Estándar")){
                tiempo.stop();
                usr=(String) combousers.getSelectedItem();
                
                esconder();
               // System.out.println(usr+","+pass+","+priv+": timer estandar");
                Pantallaprincipalestandar ps=new Pantallaprincipalestandar();
                
                ps.setVisible(true);
            //setVisible(false);
            //this.dispose();
            }*/
            /*if(cont==100){
                tiempo.stop();
                
                esconder();
                Pantallaprincipaladmin pp=new Pantallaprincipaladmin();
                
                pp.setVisible(true);
            setVisible(false);
            //this.dispose();
*/            
            
        }
        public void esconder(){
            barra.setVisible(false);
            
        }
        
        public void activar(){
            tiempo.start();
            
        }

        
    
    }//End class
    
    /*public void cargarusers(){
        Connection con=null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            con=DriverManager.getConnection("jdbc:mysql://localhost:3306","root","");
        Statement st=con.createStatement();
        ResultSet rs=st.executeQuery("select User from mysql.user;");
        combousers.removeAllItems();
        while(rs.next()){
            combousers.addItem(rs.getString(1));
        }//End while
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }//End method cargarusers*/
    
   /* public String privilegios(){
        Connection con=null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            con=DriverManager.getConnection("jdbc:mysql://localhost:3306","root","");
        Statement st=con.createStatement();
        ResultSet rs=st.executeQuery("SHOW GRANTS FOR '"+combousers.getSelectedItem()+"'@'localhost';");
        System.out.println(combousers.getSelectedItem());
       
        while(rs.next()){
           privilegios.setText(rs.getString(1)); 
           privilegios2.setText(rs.getString(1)); 
           //System.out.println(rs.getString(1));
        }//End while
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return (String) combousers.getSelectedItem();
    }//End method privilegios*/
    
    
    

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        mensajeconectado = new javax.swing.JLabel();
        mensajeerror = new javax.swing.JLabel();
        combousers = new javax.swing.JComboBox<>();
        btningresar = new javax.swing.JButton();
        contraseña = new javax.swing.JPasswordField();
        barra = new javax.swing.JProgressBar();
        privilegios3 = new javax.swing.JLabel();
        privilegios2 = new javax.swing.JLabel();
        privilegios = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Taller Automotriz - Acceso");

        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel4.setFont(new java.awt.Font("Gadugi", 1, 21)); // NOI18N
        jLabel4.setForeground(java.awt.Color.white);
        jLabel4.setText("Bienvenido al Taller Automotriz");
        jPanel1.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 30, 322, 16));

        mensajeconectado.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        mensajeconectado.setForeground(new java.awt.Color(51, 255, 51));
        jPanel1.add(mensajeconectado, new org.netbeans.lib.awtextra.AbsoluteConstraints(350, 190, 224, 18));

        mensajeerror.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        mensajeerror.setForeground(new java.awt.Color(255, 0, 0));
        jPanel1.add(mensajeerror, new org.netbeans.lib.awtextra.AbsoluteConstraints(350, 210, 212, 20));

        combousers.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                combousersItemStateChanged(evt);
            }
        });
        combousers.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                combousersMouseClicked(evt);
            }
        });
        combousers.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                combousersActionPerformed(evt);
            }
        });
        jPanel1.add(combousers, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 130, 170, -1));

        btningresar.setText("Ingresar");
        btningresar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btningresarActionPerformed(evt);
            }
        });
        jPanel1.add(btningresar, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 170, -1, -1));

        contraseña.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                contraseñaActionPerformed(evt);
            }
        });
        jPanel1.add(contraseña, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 180, 180, -1));

        barra.setFont(new java.awt.Font("Dialog", 1, 13)); // NOI18N
        barra.setForeground(new java.awt.Color(0, 0, 0));
        barra.setBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.UIManager.getDefaults().getColor("Button.darcula.selection.color1"), null));
        barra.setCursor(new java.awt.Cursor(java.awt.Cursor.WAIT_CURSOR));
        barra.setName(""); // NOI18N
        barra.setStringPainted(true);
        jPanel1.add(barra, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 220, 580, 20));

        privilegios3.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        privilegios3.setForeground(new java.awt.Color(255, 255, 255));
        jPanel1.add(privilegios3, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 100, 520, 20));

        privilegios2.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        privilegios2.setForeground(new java.awt.Color(255, 255, 255));
        jPanel1.add(privilegios2, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 80, 470, 20));

        privilegios.setBackground(new java.awt.Color(255, 255, 255));
        privilegios.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        privilegios.setForeground(new java.awt.Color(255, 255, 255));
        jPanel1.add(privilegios, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 60, 550, 20));

        jLabel2.setForeground(java.awt.Color.white);
        jLabel2.setText("User:");
        jPanel1.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 140, -1, -1));

        jLabel3.setForeground(java.awt.Color.white);
        jLabel3.setText("Password:");
        jPanel1.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 180, 80, -1));

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/secure.jpg"))); // NOI18N
        jPanel1.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, -1));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    
    
    
    
    
    private void btningresarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btningresarActionPerformed
      
        usr=(String) combousers.getSelectedItem();    
        //System.out.println(usr+","+pass+","+priv+": btningresar action");
//try{      
            //user=(String) combousers.getSelectedItem();
            //password=contraseña.getText();
            Conectar Conectando=new Conectar();
            Conectando.conexion("tallerbd");
            //System.out.println(combousers.getSelectedItem());
            
            if (contraseña.getText().equalsIgnoreCase(pass)) {
          //      System.out.println(contraseña.getText()+" ,  "+privilegios2.getText());
           // mensajeconectado.setText("Conexión establecida.");
            
            cont=-1;
            barra.setValue(0);
            barra.setStringPainted(true);
            tiempo = new Timer(TWO_SECOND,new TimerListener());
            tiempo.start();
            //System.out.println(usr+","+pass+","+priv+": btningresar action medio");
            /*  Pantallaprincipaladmin pp=new Pantallaprincipaladmin();
            pp.setVisible(true);
            this.dispose();*/
           // }catch(){}
           
        }
            else{
                JOptionPane.showMessageDialog(null,"Usuario o Contraseña no valido(s).");
            }
            //System.out.println(usr+","+pass+","+priv+": btningresar action fin");
    }//GEN-LAST:event_btningresarActionPerformed

    private void combousersActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_combousersActionPerformed
       //privilegios();
      // usr=(String) combousers.getSelectedItem();
       //System.out.println(usr+","+pass+","+priv+": combo inicio");
       privilegios.setText("Usuario Activo: "+combousers.getSelectedItem());
       //nombre=combousers.getSelectedItem().toString();
      // System.out.println("comboUsiario Activo: "+usr);
       
       try {
            Class.forName("com.mysql.jdbc.Driver");
            con=DriverManager.getConnection("jdbc:mysql://localhost/tallerbd","root","");
        Statement st=con.createStatement();
        ResultSet rs=st.executeQuery("Select Contraseña,Privilegios,Nombre_Completo from usuarios where Nickname='"+/*usr*/combousers.getSelectedItem()+"';");
        while(rs.next()){
            pass=rs.getString(1);
            priv=rs.getString(2);
            nombre=rs.getString(3);
                
                
                
               // System.out.println(rs.getString(3)+"login");
            
          
        privilegios.setText("Usuario Activo: "+usr);
        //System.out.println("Contraseña Activa: "+rs.getString(1));
        privilegios2.setText("comboContraseña Activa: "+rs.getString(1));
        
        //System.out.println("Privilegios Activos: "+rs.getString(2));
        privilegios3.setText("comboPrivilegios Activos: "+rs.getString(2));
        
        }//End while
      //  System.out.println(usr+","+pass+","+priv+": combo medio");
        //pass=rs.getString(1);
        //return rs.getString(1);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
        }
      // System.out.println(usr+","+pass+","+priv+": combo fin");
       usr=(String) combousers.getSelectedItem();
                   

       
    }//GEN-LAST:event_combousersActionPerformed

   
    
    
    private void contraseñaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_contraseñaActionPerformed
       //usr=(String) combousers.getSelectedItem();
       //System.out.println(usr+","+pass+","+priv+": contraseñaaction");
      // flag=true;
    }//GEN-LAST:event_contraseñaActionPerformed

    private void combousersItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_combousersItemStateChanged
      //  System.out.println("ItemChanged");
      
    }//GEN-LAST:event_combousersItemStateChanged

    private void combousersMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_combousersMouseClicked
        System.out.println(usr+","+pass+","+priv+": mouseclicked");
        usr=(String) combousers.getSelectedItem();
        System.out.println(usr+","+pass+","+priv+": mouseclicked2");
    }//GEN-LAST:event_combousersMouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Login().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JProgressBar barra;
    private javax.swing.JButton btningresar;
    private javax.swing.JComboBox<String> combousers;
    private javax.swing.JPasswordField contraseña;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JLabel mensajeconectado;
    private javax.swing.JLabel mensajeerror;
    private javax.swing.JLabel privilegios;
    private javax.swing.JLabel privilegios2;
    private javax.swing.JLabel privilegios3;
    // End of variables declaration//GEN-END:variables
}
