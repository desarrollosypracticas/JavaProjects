/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejm.proyectocontraseña;
import java.sql.*;
import javax.swing.JOptionPane;
/**
 *
 * @author Jose Luis Elizalde P
 */
public class Persona {
    Conexion con;
    
   public Persona(){
       con=new Conexion();
   }//End of constructor
   
   public void NuevaPersona(String name,String contra){
       try{
           PreparedStatement pstm=con.getConnection().prepareStatement("INSERT INTO "
           +"tablausers(USUARIO,CONTRASEÑA) "+" VALUES(?,?)");
           pstm.setString(1,name);
           pstm.setString(2,contra);
           pstm.execute();
           pstm.close();
           JOptionPane.showMessageDialog(null,"usuario"+" "+name+" "+"creado correctamente. ");
       }catch(SQLException e){
           System.out.println(e);
       }//End try...catch
       
   }//End of method NuevaPersona
    
}
