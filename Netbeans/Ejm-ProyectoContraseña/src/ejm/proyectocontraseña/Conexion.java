/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejm.proyectocontraseña;

import java.sql.*;
import javax.swing.JOptionPane;


public class Conexion {
    public String bd="usuarios";
    public String login="root";
    public String password="toor";
    public String url="jdbc:mysql://localhost/"+bd;
    
    Connection conn=null;
    
    public Conexion(){
        try{
            Class.forName("com.mysql.jdbc.Driver");
            conn=DriverManager.getConnection(url,login,password);
            if (conn!=null) {
                JOptionPane.showMessageDialog(null,"Conecion a base de datos "+bd+" listo.");
            }
        }catch(SQLException e){
            System.out.println(e);
        }catch(ClassNotFoundException e){
            System.out.println(e);
        }//End of try...catch
    }//End of constructor
    
    public Connection getConnection(){
        return conn;
    }//End of method getConnection
    
    public void desconectar(){
        conn=null;
    }//End of method desconectar
    
}//End of class Conexion
