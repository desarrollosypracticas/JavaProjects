/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prueba_conexion;

import java.sql.*;
import javax.swing.JOptionPane;


public class Conectar {
    Connection con=null;
    
    
    public static void main(String args[]){
        Conectar conexion=new Conectar();
        conexion.conexion("");
    }
    
    
    public Connection conexion(String BD){
        try{
            //Cargar nuestro driver
            Class.forName("com.mysql.jdbc.Driver");
            con=DriverManager.getConnection("jdbc:mysql://localhost/"+BD,"root","");//db_ejemplo
            System.out.println("Conexion Establecida.");
            JOptionPane.showMessageDialog(null,"Conexion Establecida.");
        }catch(ClassNotFoundException | SQLException e){
            System.out.println("Error de Conexion.");
            JOptionPane.showMessageDialog(null,"Error de Conexion: "+e);
        }//End of try...catch
        return con;
    }//End of method conexion
}
