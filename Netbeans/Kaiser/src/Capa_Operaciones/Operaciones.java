/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Capa_Operaciones;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import java.util.Vector;
//importamos el paquete de conexion
import Capa_Conexion.Conexion;

import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Operaciones {
    //Creamos las variables para la conexión
    static Connection cn;
    static Statement st;
    static ResultSet rs;
    DefaultTableModel modelo=new DefaultTableModel();
    Vector<String> usr=new Vector<String>();
    
    
    
    //Creamos operación para mostrar datos en un combo
    public Vector<String> cargausuarios(){
        
        usr.clear();
        try {
            cn=Conexion.Enlace(cn);
            Statement st=cn.createStatement();
            //Consulta a mostrar
            String query="SELECT nickname FROM usuarios;";
            rs=st.executeQuery(query);
            ResultSetMetaData rsmd=rs.getMetaData();
            
            //System.out.println("Size: "+usr.size());
            while(rs.next()){
            //System.out.println("1."+rs.getObject(1));
            usr.addElement((String)rs.getObject(1));
            
            //System.out.println("2."+usr.elementAt(0));
            
            }//End while
        } catch (SQLException ex) {
            Logger.getLogger(Operaciones.class.getName()).log(Level.SEVERE, null, ex);
        }catch(Exception e){
            System.out.println(e);
        }

        return usr;
}
    
   
    
    
    //Creamos la operación para mostrar datos en una jtable en el jform
    public DefaultTableModel lista(){
    
    try{
        cn=Conexion.Enlace(cn);
        Statement st=cn.createStatement();
        //Consulta a mostrar
        String query="SELECT * FROM usuarios;";
        rs=st.executeQuery(query);
        ResultSetMetaData rsmd=rs.getMetaData();
        //Obtenemos número de columnas
        int CanColumn=rsmd.getColumnCount();
        //Comprobamos 
        for (int i = 1; i < CanColumn; i++) {
            //Cargamos columnas en modelo
            modelo.addColumn(rsmd.getColumnLabel(i));
        }//End for
        
        while(rs.next()){
            //Creamos array
        Object[] fila=new Object[CanColumn];
        //Cargamos datos a modelo
            for (int i = 0; i < CanColumn; i++) {
                fila[i]=rs.getObject(i+1);
            }//End for
        modelo.addRow(fila);
        }//End while
        
        
    
    
    
    }catch(Exception e){
    
        
    
    }//End try-catch
    
//Retornamos modelo para jtable 
        return modelo;    
    }//End method DefaultTableModel
    
    
    
    //Creamos método para insertar datos
    public void AgregarConsulta(String nombre,String nickname,String privilegios,String password){
    //Dentro de try catch por si los errores
    try{
    Statement st=cn.createStatement();
    String query="INSERT INTO usuarios(nombre,nickname,privilegios,password) VALUES ('"+nombre+"','"+nickname+"','"+privilegios+"','"+password+"');";
    st.executeUpdate(query);
    st.close();
    cn.close();
    JOptionPane.showMessageDialog(null,"Registro Exitoso");
    
    }catch(Exception e){
    JOptionPane.showMessageDialog(null,e);
   
    }//End try-catch
    
    
    }//End method AgregarConsulta
    
    
    public void EliminarConsulta(String id){
        try{
        Statement st=cn.createStatement();
        String query="DELETE FROM usuarios WHERE id="+id+"";
        st.executeUpdate(query);
        st.close();
        cn.close();
        JOptionPane.showMessageDialog(null,"Eliminación Exitosa");
        
        
        
        }catch(Exception e){
        JOptionPane.showMessageDialog(null,e);
        }//End try-catch
    
    
    }//End method EliminarConsulta
    
    
    //Creamos método para modificar datos
    public void ModificarConsulta(String nombre,String nickname,String privilegios,String password){
    try{
    Statement st=cn.createStatement();
    String query="UPDATE usuarios SET nombre='"+nombre+"',nickname='"+nickname+"',privilegios='"+privilegios+"',password='"+password+"'";
    st.executeUpdate(query);
    st.close();
    cn.close();
    }catch(Exception e){
    JOptionPane.showMessageDialog(null,e);
    }//End try-catch
    
    }//End method ModificarConsulta
    
}
