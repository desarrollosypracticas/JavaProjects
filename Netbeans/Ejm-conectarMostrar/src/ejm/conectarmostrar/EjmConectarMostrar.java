/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ejm.conectarmostrar;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import jdk.nashorn.internal.ir.Statement;

/**
 *
 * @author Jose Luis
 */
public class EjmConectarMostrar {

    private static Connection conexion=null;
    private static String bd="agenda";//Nombre de BD.
    private static String user="root";//Usuario de BD.
    private static String password="";//Password de BD.
    
    //Driver para MySQL en este caso.
    private static String driver="com.mysql.jdbc.Driver";
    //Ruta del servidor.
    private static String server="jdbc:mysql://localhost/"+bd;
    
    
    public static void main(String[] args) throws SQLException {
        
        System.out.println("INICIO DE EJECUCIÓN.");
        conectar();
        Statement st=conexion();
        
        //Se elimina la tabla "personal" en caso de existir
        String cadena="DROP TABLE IF EXISTS personal;";
        consultaActualiza(st,cadena);
        
        //Se crea la tabla "personal"
        cadena="CREATE TABLE personal ('Identificador' int(11) NOT NULL "
                + "AUTO_INCREMENT,'Nombre' VARCHAR(50) NOT NULL,'Apellidos' VARCHAR(50) NOT NULL,"
                + "'Telefono' VARCHAR(9) DEFAULT NULL,'Email' VARCHAR(60) DEFAULT NULL,"
                + "PRIMARY KEY('Identificador'));";
        consultaActualiza(st,cadena);
        
        //Se crean datos de prueba para utilizarlos en la tabla "personal"
        cadena="INSERT INTO personal('Identificador','Nombre','Apellidos','Telefono','Email'"
                + "VALUES (1,'José','Martínez López',´968112233´,'jose@martinezlopez.com'),"
                + "(2,'María','Gómez Muñoz','911876876','maria@gomezoliver.com'),"
                + "(3,'Juan','Sánchez Fernández','922111333','juan@sanchezfernandez.com'),"
                + "(4,'Ana','Murcia Rodríguez','950999888','ana@murciarodriguez.com'));";
        consultaActualiza(st,cadena);
        
        //Se sacan los datos de la tabla personal
        cadena="SELECT * FROM personal";
        ResultSet rs=consultaQuery(st,cadena);
        if (rs!=null) {
            System.out.println("El listado de persona es el siguiente: ");
            while(rs.next()){
                System.out.println("    ID: "+rs.getObject("Identificador"));
                System.out.println("    Nombre Completo:    "+rs.getObject("Nombre")
                +"  "+rs.getObject("Apellidos"));
                
                System.out.println("    Contacto:   "+rs.getObject("Telefono")
                +"  "+rs.getObject("Email"));
                
                System.out.println("-   ");
            }//End while
            cerrar(rs);
        }//End if
        cerrar((ResultSet) st);
        System.out.println("FIN DE EJECUCIÓN.");
    }//End of main.
    
    /*Método necesario para conectarse al Driver y poder usar MySQL.*/
    public static void conectar(){
        try{
            Class.forName(driver);
            conexion=DriverManager.getConnection(server,user,password);
            
        }catch(Exception e){
            System.out.println("Error: Imposible realizar la conexión a BD.");
            e.printStackTrace();
        }//End of try...catch
    }//End of method conectar
    
    /*Método para establecer la conexión con la base de datos.*/
    private static Statement conexion() {
        Statement st = null;
        try {
            st = conexion.createStatement();
        } catch (SQLException e) {
            System.out.println("Error: Conexión incorrecta.");
            e.printStackTrace();
        }
        return st;
    }//End of method conexion
    
    /*Método para realizar consultas de actualización, creación o eliminación.*/
    private static int consultaActualiza(Statement st,String cadena){
        int rs=-1;
        try{
            rs=st.executeUpdate(cadena);
            
        }catch(SQLException e){
            System.out.println("Error con: "+cadena);
            System.out.println("SQLException: "+e.getMessage());
            e.printStackTrace();
        }//End of try...catch
        return rs;
    }//End of method consultaActualiza
    
    /*Método para realizar consultas del tipo: SELECT * FROM tabla WHERE...*/
    private static ResultSet consultaQuery(Statement st,String cadena){
        ResultSet rs=null;
        try{
            rs=st.executeQuery(cadena);
            
        }catch(SQLException e){
            System.out.println("Error con: "+cadena);
            System.out.println("SQLException: "+e.getMessage());
            e.printStackTrace();
        }//End of try...catch
        return rs;
    }//End of method consultaQuery
    
    /*Método para cerrar la consulta*/
    private static void cerrar(ResultSet rs){
        if (rs!=null) {
            try{
                rs.close();
                
            }catch(Exception e){
                System.out.println("Error: No es posible cerrar la consulta.");
            }//End try...catch
        }
    }//End of method cerrar
    
    /*Método para cerrar la conexión.*/
    private static void cerrar(java.sql.Statement st){
        if (st!=null) {
            try{
                st.close();
            }catch(Exception e){
                System.out.println("Error: No es posible cerrar la conexión.");
            }//End try...catch
        }
    }//End of method cerrar
    
}//End of class
