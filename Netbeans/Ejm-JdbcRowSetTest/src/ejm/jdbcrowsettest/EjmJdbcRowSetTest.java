/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejm.jdbcrowsettest;

import com.sun.rowset.JdbcRowSetImpl;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import javax.sql.rowset.JdbcRowSet;

/**
 *
 * @author Jose Luis
 */
public class EjmJdbcRowSetTest {

    /*JDBC driver name and database URL*/
    static final String DATABASE_URL="jdbc:mysql://localhost/prueba";
    static final String USERNAME="root";
    static final String PASSWORD="";
    
    //Constructor connects to database, queries database, processes
    //results and display results in window
    
    public EjmJdbcRowSetTest(){
        //Connect to database sistema and query database
        try{
            //Specify properties od JdbcRowSet
            JdbcRowSet rowSet=new JdbcRowSetImpl();
            rowSet.setUrl(DATABASE_URL);//Set database URL
            rowSet.setUsername(USERNAME);//Set username
            rowSet.setPassword(PASSWORD);//Set Password
            rowSet.setCommand("SELECT * FROM usuarios");//Set query
            
            //Process query results
            ResultSetMetaData metaData=rowSet.getMetaData();
            int numberOfColumns=metaData.getColumnCount();
            System.out.println("Usuaios Table of sistema Database: \n");
            
            //display rowset header
            while(rowSet.next()){
            for (int i = 1; i <= numberOfColumns; i++) 
                System.out.printf("%-8s\t",rowSet.getObject(i));
                System.out.println();
            }//End while
            
            //Close the underlying ReultSet, Statement and Connection
            rowSet.close();
            
        }catch(SQLException sqlException){
            sqlException.printStackTrace();
            System.exit(1);
        }//End try...catch
    }//End constructor
    
    
    //Launch the application
    public static void main(String[] args) {
        EjmJdbcRowSetTest application=new EjmJdbcRowSetTest();
    }//End main
    
}//End class JdbcRowSetTest
