

/*
Programa de prueba para conexión a una base de datos de MySQL.
Presupone que el servidor de base de datos está arrancado, disponible,
en el puerto por defecto.
El usuario y password de conexión con la base de datos debe cambiarse.
En la base de datos se supone que hay una base de datos llamada prueba y que
tiene una tabla persona con tres campos, de esta manera:
mysql>create database pruebaconexion;
mysql>use pruebaconexion;
mysql>CREATE TABLE persona (id smallint auto_incremenT,nombre varchar(60),
    nacimiento date, primary key(id));
*/
package ejm.pruebamysql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import jdk.nashorn.internal.ir.Statement;


/*
    Clase de prueba de conexión con una base de datos MySQL.
*/
public class EjmPruebaMySQL {

    /*
        Crea una instancia de la clase MySQL y realiza todo el código
    de conexión, consulta y muestra de resultados.
    */
    
    public EjmPruebaMySQL(){
    
        //Se mete todo en un try por los posibles errores de MySQL
        try{
            //Se registra el Driver de MySQL
            //DriverManager.registrerDriver(new org.gjt.mm.mysql.Driver());
                        Class.forName("com.mysql.jdbc.Driver");

            
            /*
            Se obtiene una conexión con la base de datos. Hay que
            cambiar el usuario "root" y la clave "la_clave" por las 
            adecuadas a la base de datos que estemos usando.
            */
            Connection conexion=DriverManager.getConnection("jdbc:mysql://localhost/prueba","root","");
            
            /*Se crea un Statement, para realizar la consulta.*/
            Statement s = conexion.createStatement();
            
            /*Se realiza la consulta. Los resultados se guardan en el ResultSet rs
            */
            ResultSet rs=s.executeQuery("SELECT * FROM persona");
            
            /*Se recorre el ResultSet, mostrando por pantalla los resultados.*/
            while(rs.next()){
                System.out.println(rs.getInt("Id")+" "+rs.getInt(2)+" "+rs.getInt(3));
            }//End while
            
            //Se cierra la conexión con la base de datos.
            conexion.close();
            
        
        }catch(Exception e){
            e.printStackTrace();
        }//End try...catch
    }//End constructor
    
    
    public static void main(String[] args) {
        new EjmPruebaMySQL();
    }
    
}
