/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejm.eventomouse;

import java.awt.event.MouseListener;
import javafx.scene.input.MouseEvent;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

/**
 *
 * @author Jose Luis
 */
class Ventana extends JFrame implements MouseListener{
    
    JLabel etiqueta1,etiqueta2;
    JLabel labelTitulo;
    JButton boton1;
    
    public Ventana(){
        setLayout(null);
        /*Propiedades del label, lo instanciamos, posicionamos y activamos los eventos.*/
        labelTitulo=new JLabel();
        labelTitulo.setText("Eventos del Mouse.");
        labelTitulo.setFont(new java.awt.Font("Comic Sans MS",0,28));
        labelTitulo.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        labelTitulo.setBorder(javax.swing.BorderFactory.createBevelBorder
        (javax.swing.border.BevelBorder.LOWERED));
        labelTitulo.setBounds(40,10,300,40);
        labelTitulo.addMouseListener(this);
        
        etiqueta1=new JLabel();
        etiqueta1.setBounds(10,160,190,20);
        etiqueta2=new JLabel();
        etiqueta2.setBounds(10,180,190,20);
        
        boton1=new JButton();
        boton1.setBounds(110,75,150,75);
        boton1.setText("Presioname");
        boton1.addMouseListener(this);
        
        add(labelTitulo);
        add(etiqueta1);
        add(etiqueta2);
        add(boton1);
        
        setTitle("Eventos del Mouse");
        setSize(400,240);
    }//End constructor
    
    public void mouseClicked(java.awt.event.MouseEvent e) {
        
        if (e.getSource()==boton1) {
            etiqueta1.setText("Hizo clic en mi botón.");
        }
        if (e.getSource()==labelTitulo) {
            etiqueta1.setText("Hizo clic en el Titulo.");
        }
        
    }//End method mouseClicked

    
    public void mousePressed(java.awt.event.MouseEvent e) {
        
        etiqueta1.setText("");
        if (e.getSource()==boton1) {
            etiqueta2.setText("Presiono el botón.");
        }
        if (e.getSource()==labelTitulo) {
            etiqueta2.setText("Presiono el Titulo.");
        }
    }//End method mousePressed
    
    public void mouseReleased(java.awt.event.MouseEvent evento){
        if (evento.getSource()==boton1) {
            etiqueta2.setText("Libero el botón");
        }
        if (evento.getSource()==labelTitulo) {
            etiqueta2.setText("Libero el Titulo.");
        }
    }//End method mouseReleased
    
    public void mouseExited(java.awt.event.MouseEvent evento){
        if (evento.getSource()==boton1) {
            etiqueta1.setText("Salio del botón.");
        }
        if (evento.getSource()==labelTitulo) {
            etiqueta1.setText("Salio del Titulo.");
        }
        etiqueta2.setText("");
    }//End method mouseExited
    
    public void mouseEntered(java.awt.event.MouseEvent evento){
        if (evento.getSource()==boton1) {
            etiqueta1.setText("Entro a mi botón.");
        }
        if (evento.getSource()==labelTitulo) {
            etiqueta1.setText("Entro al Titulo.");
        }
        etiqueta2.setText("");
    }//End method mouseEntered

   
    


    
}
