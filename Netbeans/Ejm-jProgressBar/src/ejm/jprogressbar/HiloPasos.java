/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejm.jprogressbar;

import java.awt.EventQueue;
import java.lang.reflect.InvocationTargetException;
import javax.swing.JProgressBar;

/**
 *
 * @author Jose Luis
 */
public class HiloPasos extends Thread{
    JProgressBar barra;
    
    private static int retraso=300;
    
    public HiloPasos(JProgressBar barra){
        this.barra=barra;
    }//End constructor
    
    public void run(){
        int minimo=barra.getMinimum();
        int maximo=barra.getMaximum();
        
        Runnable ejecutor=new Runnable(){
            @Override
            public void run() {
                int valorActual=barra.getValue();
                barra.setValue(valorActual+1);
            }//End run
            
            
        };//End Runnable
        
        for (int i = minimo; i < maximo; i++) {
            try{
                EventQueue.invokeAndWait(ejecutor);
                Thread.sleep(retraso);
            }catch(InterruptedException ex){}
            catch(InvocationTargetException ex){}//End try...catch
        }//End for
        
        
    }
    
    
}
